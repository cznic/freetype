# freetype

CGo-free port of libfreetype from https://www.freetype.org/

## Installation

    $ go get modernc.org/freetype

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/freetype/lib

## Documentation

[godoc.org/modernc.org/freetype](http://godoc.org/modernc.org/freetype)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2ffreetype](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2ffreetype)
